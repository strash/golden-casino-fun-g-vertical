extends Control


# описание видимости блоков GUI
const GUI_SETUP: Dictionary = {
	"is_header_block_visible": true,
	"is_footer_block_visible": false,
	"is_score_block_visible": true,
	"is_level_block_visible": true,
	"is_btn_back_block_visible": true,
}

var payload: Dictionary = {} setget set_payload


const TEXT: PoolStringArray = PoolStringArray([
	"The Story of Play’n GO\n\nIn the mid 90s, we were just a small group of computer geeks who made our livings as consultants to the cutting-edge corporations of the day. We eventually realized we could pool our talents to offer companies more entertaining and reliable software and systems ourselves, so we started Play’n GO.\n\nOur goal is simply to delight others and inspire them to have fun. This positive approach creates an upbeat, cooperative work environment and enhances our direct relationships with clients, and perhaps more importantly, our indirect relationships with their fun-seeking players through the games we create for them.\n\nPlay’n GO has grown substantially over the past two decades. Today, we have many more talented team members who share the vision to create top-notch entertainment by utilizing each other’s skills as we enjoy each other’s company to augment the innovative process.",
	"We deliver high-end, next level gaming to the slots market. Offering an established portfolio that encompasses uniquely themed games, innovative features and volatilities to satisfy every spectrum of the player market. We utilise the knowledge of seasoned experts, who apply their years of expertise in the industry to craft games to the highest of standards, harnessing proven formulas for success, combined with the latest developments in the gaming industry.",
	"Evoplay is an award-winning iGaming development studio. We do things a little differently. We think we do them better. That means more than just making great games. It means providing the best service behind those games, too.\n\nWe continue to build on our incredible portfolio – from first-to-market innovations to beautiful versions of the games people know and love. We provide online casinos with video slots, table, classic and instant games.",
	"Entertaining players through the supply of classic and tailored content, real money, and social operations – this is what defines Greentube as the leading full-service provider in the online and mobile gaming sector. Greentube prides itself on the delivery of safe and secure casino entertainment to players around the world. As the NOVOMATIC Interactive division, we provide solutions that go above and beyond and shape the future of this ever-changing sector. Greentube’s vast and continuously growing portfolio of impressive products and services includes classic slots, video bingo games, video poker.",
	"Pragmatic Play is a leading content provider for the iGaming industry, offering a multi-product portfolio that is innovative, regulated and mobile-focused. Our passion for premium entertainment is unmatched. We strive to create the most engaging and memorable experience for all of our customers across a range of products including slot machines, live casinos and bingo.",
])


# BUILTINS -------------------------


# METHODS -------------------------


func prepare_view(level: int) -> void:
	($BG as TextureRect).texture = Global.BGS[level - 1]
	($Scroll/Margin/List/Text as Label).text = TEXT[level - 1]
	($Scroll/Margin/List/Msg1/Eblo as TextureRect).texture = load("res://Resources/Sprites/eblo_1_lvl_%s-min.png" % level)
	($Scroll/Margin/List/Msg2/Eblo as TextureRect).texture = load("res://Resources/Sprites/eblo_2_lvl_%s-min.png" % level)


# SETGET -------------------------


func set_payload(dict: Dictionary) -> void:
	payload = dict
	prepare_view(payload.level)


# SIGNALS -------------------------


